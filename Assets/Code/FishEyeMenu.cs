﻿using System.Collections.Generic;
using UnityEngine;

public class FishEyeMenu : MonoBehaviour {
    [Header("Menu Settings")]
    [Range(0, 500)]
    [SerializeField] float spacing = 5f;

    [Range(0f, 20f)]
    [SerializeField] float snapSpeed = 5f;

    [Range(0f, 10f)]
    [SerializeField] float scaleOffset = 5f;

    [Range(1f, 20f)]
    [SerializeField] float scaleSpeed = 5f;

    [Range(0, 1)]
    [SerializeField] float minScale = 0.5f;

    [Range(1, 5)]
    [SerializeField] float maxScale = 1.5f;

    [SerializeField] bool inertia = true;

    [Range(0, 10)]
    [SerializeField] float inertiaTime = 5f;

    [Header("References")]
    [SerializeField] List<GameObject> objectPrefabs = new List<GameObject>();
    [SerializeField] Transform contentParent;

    GameObject[] instantiatedObjects;
    Vector2[] objectsPosition;

    Vector2 contentPosition;
    Vector3 touchOffset;
    Vector3 prevPosition;
    Vector3 dropPosition;
    Vector3 contentDistance;
    Vector3 velocity;

    float time;

    int objectsCount;
    int id;

    bool isDragging;
    bool startInertia;

#region unity_methods
    void Awake() {
        Initialize();
    }

    void OnEnable() {
        AttachEventHandlers();
    }

    void Start() {
        SpawnObjects();
    }

    void FixedUpdate() {
        ControllMenuElements();
    }

    void Update() {
        InertialMovement();
    }

    void OnDisable() {
        RemoveEventHandlers();
    }
#endregion

    void Initialize() {
        objectsCount = objectPrefabs.Count;
        instantiatedObjects = new GameObject[objectsCount];
        objectsPosition = new Vector2[objectsCount];
        contentDistance = Camera.main.WorldToScreenPoint(contentParent.position);
    }

    void AttachEventHandlers() {
        Swipe.Instance.OnTapDown += OnMouseDown;
        Swipe.Instance.OnDrag += OnMouseDrag;
        Swipe.Instance.OnTapUp += OnMouseUp;
    }

    void OnMouseDown() {
        touchOffset =
            Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y,
                Camera.main.WorldToScreenPoint(contentParent.position).z)) - contentParent.position;
        startInertia = false;
    }

    void OnMouseDrag() {
        isDragging = true;
        prevPosition = contentParent.position;
        Vector3 movePosition =
            Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y,
                contentDistance.z));

        contentParent.position = new Vector3(movePosition.x - touchOffset.x, contentParent.position.y,
            movePosition.z - touchOffset.z);
    }

    void OnMouseUp() {
        velocity = contentParent.position - prevPosition;
        if (inertia) {
            startInertia = true;
            return;
        }
        isDragging = false;
    }


    void SpawnObjects() {
        for (int i = 0; i < objectsCount; i++) {
            instantiatedObjects[i] = Instantiate(objectPrefabs[i], contentParent, false);
            if (i == 0) {
                continue;
            }
            instantiatedObjects[i].transform.position =
                new Vector2(
                    instantiatedObjects[i - 1].transform.position.x + spacing,
                    instantiatedObjects[i].transform.position.y);
            objectsPosition[i] = -instantiatedObjects[i].transform.position;
        }
    }
    void ControllMenuElements() {
        ModifyObjects();

        if (isDragging) {
            return;
        }

        SnapMenu();
    }

    void ModifyObjects() {
        float nearestPos = float.MaxValue;
        for (int i = 0; i < objectsCount; i++) {
            float distance = Mathf.Abs(transform.position.x - instantiatedObjects[i].transform.position.x);
            if (distance < nearestPos) {
                nearestPos = distance;
                id = i;
            }
            Vector3 scale = Mathf.Clamp(1/(distance)*scaleOffset, minScale, maxScale) * Vector3.one;
            Vector3 objectScale = Vector3.Lerp(instantiatedObjects[i].transform.localScale, scale,
                scaleSpeed*Time.fixedDeltaTime);
            instantiatedObjects[i].transform.localScale = objectScale;
        }
    }

    void SnapMenu() {
        contentPosition = Vector3.Lerp(contentParent.position, objectsPosition[id],
            snapSpeed*Time.fixedDeltaTime);
        contentParent.position = contentPosition;
    }
    
    void InertialMovement() {
        if (inertia && startInertia && time <= inertiaTime) {
            ApplyInertia();
        }
        else {
            StopMovement();
        }
    }

    void ApplyInertia() {
        contentParent.position += velocity;
        velocity = Vector3.Lerp(velocity, Vector3.zero, time);
        time += Time.smoothDeltaTime;
    }

    void StopMovement() {
        isDragging = false;
        startInertia = false;
        velocity = Vector3.zero;
        time = 0.0f;
    }

    void RemoveEventHandlers() {
        if (Swipe.Instance.OnTapDown != null) {
            Swipe.Instance.OnTapDown -= OnMouseDown;
        }

        if (Swipe.Instance.OnDrag != null) {
            Swipe.Instance.OnDrag -= OnMouseDrag;
        }

        if (Swipe.Instance.OnTapUp != null) {
            Swipe.Instance.OnTapUp -= OnMouseUp;
        }
    }

    public GameObject GetSelectedObject() {
        return instantiatedObjects[id];
    }
}